#!/usr/bin/python

import ConfigParser
import sys
import os

class ConfigManager:
    DEFAULT_CONFIG_FILENAME = 'config.ini'
    def __init__(self, config_filename=None):
        self.config_parser = ConfigParser.ConfigParser()
        default = self.default()
        for section in default:
            self.config_parser.add_section(section)
            for (key, value) in default[section].iteritems():
                self.config_parser.set(section, key, value)
        if config_filename:
            self.config_filename = config_filename
            if os.path.exists(self.config_filename):
                self.config_parser.read(self.config_filename)
        else:
            self.config_filename = ConfigManager.DEFAULT_CONFIG_FILENAME

    def get(self, section):
        section_dict = {}
        for (key, value) in self.config_parser.items(section):
            section_dict[key] = value
        return section_dict
        
    def get_all(self):
        config = {}
        for section in self.config_parser.sections():
            config[section] = {}
            for (key, value) in self.get(section).iteritems():
                config[section][key] = value
        return config

    def set(self, config, overwritten=True):
        for section in config:
            if not self.config_parser.has_section(section):
                self.config_parser.add_section(section)
            for (key, value) in config[section].iteritems():
                if (overwritten or
                    not self.config_parser.has_option(section, key)):
                    self.config_parser.set(section, key, value)

    def export(self, save_as=None, overwritten=False):
        if save_as:
            output_filename = save_as
        else:
            output_filename = self.config_filename
        if not overwritten and os.path.exists(output_filename):
            export_config = ConfigManager(output_filename)
            export_config.set(self.get_all(), overwritten=False)
            export_config.export(overwritten=True)
        else:
            f = open(output_filename, 'w')
            self.config_parser.write(f)

    def default(self):
        return {'section1':{'item1': 'v1', 'item2': 'v2', 'item3': 'v3'}}

def main():
    config = ConfigManager()
    config.export()
    return

if __name__ == "__main__":
    main()
